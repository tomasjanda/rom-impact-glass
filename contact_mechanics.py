# Basic calculations of contact force and contact area between sphere and half-space.
# https://en.wikipedia.org/wiki/Contact_mechanics#Contact_between_a_sphere_and_a_half-space

import math


def get_a(R, d):
    """
    Returns radius of contact area.
    See https://en.wikipedia.org/wiki/Contact_mechanics#Contact_between_a_sphere_and_a_half-space.
    :param R: Radius of sphere
    :param d: Indentation depth
    :return: Radius of contact circle a
    """
    return math.sqrt(R * d)


def get_F(E_star, R, d):
    """
    Returns contact force between sphere and half-space.
    See https://en.wikipedia.org/wiki/Contact_mechanics#Contact_between_a_sphere_and_a_half-space.
    :param E_star: Composite modulus
    :param R: Sphere radius
    :param d: Indentation depth
    :return: Contact force
    """
    return 4.0 / 3.0 * E_star * R**0.5 * d**1.5


def get_d(E_star, R, F):
    """
    Returns indentation depth for given contact force. (d is expressed from get_F relation)
    :param E_star: Composite modulus
    :param R: Sphere radius
    :param F: Contact force
    :return: Indentation depth
    """
    return (3.0 / 4.0 * F / E_star / R**0.5)**(2.0 / 3.0)


def get_E_star(E1, nu1, E2, nu2):
    """
    Returns composite modulus for contact between sphere and half-space.
    See https://en.wikipedia.org/wiki/Contact_mechanics#Contact_between_a_sphere_and_a_half-space.
    :param E1: Young's modulus of first body
    :param nu1: Poisson's number of first body
    :param E2: Young's modulus of second boydy
    :param nu2: Poisson's number of second body
    :return: Composite modulus E_star
    """
    return 1.0 / ((1.0 - nu1*nu1)/E1 + (1.0 - nu2*nu2)/E2)


def get_p0(F, a):
    """
    Returns maximum contact pressure (at the centre of the impact area).
    See https://en.wikipedia.org/wiki/Contact_mechanics#Contact_between_a_sphere_and_a_half-space.
    :param F: Contact force
    :param a: Radius of contact area
    :return: Maximum contact pressure
    """
    return 3.0 * F / 2.0 / math.pi / a / a

# Our case with small glass plate (values from Overleaf)
# Radius
R = 0.05 # m
# Maximum contact force (chart "contact force of the small plate" in Petr's section, Overleaf)
F = 14.5e3 # N
# Steel
E_s = 210e9 # Pa
nu_s = 0.3
# Glass
E_g = 72.0e9 # Pa
nu_g = 0.22

# Composite modulus
E_star = get_E_star(E_s, nu_s, E_g, nu_g)
print('Composite modulus E_star = {:e} [Pa]'.format(E_star))

# Indentation depth
d = get_d(E_star, R, F)
print('Indentation depth d = {:e} [m]'.format(d))

# Contact area
a = get_a(R, d)
print('Radius of contact area a = {:e} [m]'.format(a))

# Maximum contact pressure
p0 = get_p0(F, a)
print('Maximum contact pressure p0 = {:e} [Pa]'.format(p0))