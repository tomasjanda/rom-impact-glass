# 1D bar, eigenvalue analysis, rozklad do vlastnich cisel
# THIS CODE WAS MOVED TO JUPYTER NOTEBOOK!

import math
import numpy as np
import scipy
import scipy.linalg
import scipy.sparse
import matplotlib.pyplot as plt


# Num nodes
n = 6

# Model parameters
E = 1.0
A = 1.0
rho = 1.0
l = 1.0

# Nodal coordinates
x = [i*l for i in range(n)]
print('Nodal coordinates')
print(x)

# Stiffness matrix
k = E*A/l
K = scipy.sparse.diags([-k, 2*k, -k], [-1, 0, 1], shape=(n, n)).toarray()
K[0,0] = k
K[n-1, n-1] = k
print('Matrix K')
print(K)

# Mass matrix
m = rho*A*l/2
M = scipy.sparse.diags([2*m], [0], shape=(n, n)).toarray()
M[0, 0] = m
M[n-1, n-1] = m
print('Matrix M')
print(M)

I = np.identity(n)

# Standard eigenvalue problem
# lam, Q = scipy.linalg.eig(K)

# Solving generalized eigenvalue problem (A - lam * B) * v = 0
# https://en.wikipedia.org/wiki/Eigendecomposition_of_a_matrix#Generalized_eigenvalue_problem
lam, Q = scipy.linalg.eig(K, M)
print('Vector of eigenvalues (lam)')
print(lam)
print('Matrix of column eigenvectors (eigenmodes)')
print(Q)
plt.matshow(Q)
plt.title('Matrix of eigenvectors (columns)')
plt.colorbar()
plt.show()
# The i-th eigenvalue and i-th eigenmode
# lam[i]
# Q[:, i] # i-th column

# Check the solution
i = 0
lhs = np.matmul(K - lam[i] * M, Q[:, i])
print("Left hand side of equation (K - lam[i] M) u[i] = 0 (this should be zero vector up to machine precision)")
print(lhs)


def get_generalized_mass(Q, M, i):
    """
    Returns generalized mass associated with i-th eigenmode.
    It is defined in
    https://classes.engineering.wustl.edu/2009/spring/mase5513/abaqus/docs/v6.6/books/stm/default.htm?startat=ch02s05ath25.html
    as m_i = v_iT * M * v_i.
    """
    return np.matmul(Q[:, i].transpose(), np.matmul(M, Q[:, i]))


def get_modal_participation_factor(Q, M, i):
    """
    Returns generalized mass associated with i-th eigenmode.
    It is defined in
    https://classes.engineering.wustl.edu/2009/spring/mase5513/abaqus/docs/v6.6/books/stm/default.htm?startat=ch02s05ath25.html
    as Gamma_ij = 1 / mi * v_iT * M * T_j.
    For 1D rod the T_j is column vector of all ones.
    """
    n = np.shape(M)[0]
    T = np.ones(n)
    m = get_generalized_mass(Q, M, i)
    return 1 / m * np.matmul(Q[:, i].transpose(), np.matmul(M, T))


# Matrix of normalized eigenvectors
# v_norm[i] = v[i] / (vT[i] * M * v[i])
# This is not necessary. Function eig() seems to return normalized eigenvectors.
Q_norm = np.empty((n,n))
for i in range(n):
    Q_norm[:, i] = Q[:, i] / math.sqrt(np.matmul(Q[:, i].transpose(), np.matmul(M, Q[:, i])))
plt.matshow(Q_norm)
plt.title('Matrix of NORMALIZED eigenvectors (columns)')
plt.colorbar()
plt.show()

# Transformed stiffness matrix (spectral stiffness matrix?)
K_sp = np.matmul(Q_norm.transpose(), np.matmul(K,Q_norm))
print('Spektral stiffness matrix K_sp. This should be diagonal.')
print(K_sp)
plt.matshow(K_sp)
plt.title('Spectral stiffness matrix K_sp')
plt.colorbar() # Adds color scale.
plt.show()

# Transformed mass matrix (spectral mass matrix?)
M_sp = np.matmul(Q_norm.transpose(), np.matmul(M,Q_norm))
print('Spektral mass matrix M_sp. This should be diagonal.')
print(M_sp)
plt.matshow(M_sp)
plt.title('Spectral mass matrix M_sp')
plt.colorbar() # Adds color scale.
plt.show()

# Print generalized masses
generalized_mass_list = [get_generalized_mass(Q, M, i) for i in range(n)]
print('List of generalized masses.')
print(generalized_mass_list)

# Print modal participation factors
modal_participation_factors_list = [get_modal_participation_factor(Q, M, i) for i in range(n)]
print('List of modal participation factors.')
print(modal_participation_factors_list)

# Show first few eigenmodes
for i in range(min(n,6)):
    plt.plot(x, Q[:,i])
plt.title('Eigenmodes')
plt.show()

# Show first few NORMALIZED eigenmodes
for i in range(min(n,6)):
    plt.plot(x, Q_norm[:,i])
plt.title('NORMALIZED eigenmodes')
plt.show()

