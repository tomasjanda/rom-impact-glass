# Modal analysis of unconstrained elastic plate
# Computes several lowest eigenfrequencies and eigenvectors
# Close-to-zero frequencies correspond to rigid body modes.

# The plate is modeled with solid elements.

import fenics as fe
import numpy as np
import matplotlib
matplotlib.use('TkAgg') # MUST BE CALLED BEFORE IMPORTING plt
import matplotlib.pyplot as plt

# Dimensions [m]
# Lx, Ly, Lz = 1., 0.005, 0.01 # Beam
Lx, Ly, Lz = 0.5, 0.5, 0.019 # Plate. The z-axis is perpendicular to the plate.
#Lx, Ly, Lz = 1.5, 1.5, 0.01 # Plate. The z-axis is perpendicular to the plate.

# Define (box)mesh on the rectangular cuboid domain.
# Nx = 50
# Ny = Nx
# Nz = int(Lz/Lx*Nx)+1
Nx, Ny, Nz = 4, 4, 2
mesh = fe.BoxMesh(fe.Point(0., 0., 0.), fe.Point(Lx, Ly, Lz), Nx, Ny, Nz)

# Other material parameters
E, nu = 70.0e9, 0.23
rho = 2500.0

# Lamé coefficient for constitutive relation
mu = E/2./(1+nu)
lmbda = E*nu/(1+nu)/(1-2*nu)

# Weight
total_mass=Lx*Ly*Lz*rho

# FORMS
def eps(v):
    return fe.sym(fe.grad(v))


def sigma(v):
    dim = v.geometric_dimension()
    return 2.0*mu*eps(v) + lmbda*fe.tr(eps(v))*fe.Identity(dim)

V = fe.VectorFunctionSpace(mesh, 'Lagrange', degree=2)
u_ = fe.TrialFunction(V)
du = fe.TestFunction(V)


# Boundary conditions
# def left(x, on_boundary):
#    return near(x[0],0.)

# bc = DirichletBC(V, Constant((0.,0.,0.)), left)

def k(du, u_):
    return fe.inner(sigma(du),eps(u_))*fe.dx

#k_form = k(du, u_)
#K = fe.PETScMatrix()
#fe.assemble(k_form, tensor=K)

k_form = k(du, u_)
l_form = fe.Constant(1.)*u_[0]*fe.dx
K = fe.PETScMatrix()
b = fe.PETScVector()
fe.assemble_system(k_form, l_form, [], A_tensor=K, b_tensor=b)

m_form = rho*fe.dot(du,u_)*fe.dx
M = fe.PETScMatrix()
fe.assemble(m_form, tensor=M)

delta = fe.PointSource(V, fe.Point(Lx/2., Ly/2., 0.), 1.0)
delta.apply(b)

K_array = np.array(K.array()) # Converts PETScMatrix to 2D numpy array.
M_array = np.array(M.array())
b_array = np.array(b)
print("Vector b:")
print("shape = {}".format(b_array.shape))
print("(min, max) = {}".format((np.amin(b_array), np.amax(b_array))))
print("Distinct values: {}".format(set(b_array)))

#plt.plot(b_array)
#plt.show()

print("K_array:")
print(K_array.shape)
print(K_array)
print("M_array:")
print(M_array.shape)
print(M_array)
num_dof = K_array.shape[0]



eigensolver = fe.SLEPcEigenSolver(K, M)
# Set parameters
# https://fenicsproject.org/docs/dolfin/1.4.0/python/programmers-reference/cpp/la/SLEPcEigenSolver.html
eigensolver.parameters['problem_type'] = 'gen_hermitian'
# “power”, “subspace”, “arnoldi”, “lanczos”, “krylov-schur”, “lapack”, “arpack”
#eigensolver.parameters['solver'] = 'power'
eigensolver.parameters["spectrum"] = "smallest real"
eigensolver.parameters['spectral_transform'] = 'shift-and-invert'
eigensolver.parameters['spectral_shift'] = 0.


N_eig = 100   # number of eigenvalues
#N_eig = num_dof

# Solve the eigenvalue problem
print("Computing %i first eigenvalues..." % N_eig)
eigensolver.solve(N_eig)


print("Number of converged: {}".format(eigensolver.get_number_converged()))

# Set up file for exporting results
file_results = fe.XDMFFile('modal_analysis.xdmf')
#   file_results.parameters['flush_output'] = True
file_results.parameters['functions_share_mesh'] = True # Jarda: Bez toho roste velikost souboru (?)

# Define empty vector to store eigenvalues and empty matrix to store eigenmodes.

lam_vec = np.zeros(N_eig)
S_mat = np.zeros((num_dof, N_eig))

# Jen jedna instance, ulozit s jinym casem, lze animovat.
eigenmode = fe.Function(V,name="Eigenvector ")


# Extraction
for i in range(0, N_eig): # range(N_eig):
    # Extract eigenpair
    r, c, rx, cx = eigensolver.get_eigenpair(i)

    # Store lambda in the vector and eigenmode in the matrix
    lam_vec[i] = r
    S_mat[:, i] = rx

    # i-th eigenfrequency
    omega = fe.sqrt(r)

    print()
    print('Eigenfrequency no. {0}: omega = {1:8.5f} [rad/s]'.format(i, omega))
    print('Eigenfrequency no. {0}: f = {1:8.5f} [Hz]'.format(i, omega/2.0/np.pi))


    # Initialize function and assign eigenvector (renormalize by stiffness matrix)
    #eigenmode = fe.Function(V,name="Eigenvector "+str(i))
    eigenmode.vector()[:] = rx

    # Displacement of the middle point of the beam in direction of z-axis.
    disp_z_mid = eigenmode(Lx/2, Ly/2, Lz/2)[2]
    # Elastic energy stored in the material when the beam is deformed to given eigenmode.
    E_elas = fe.assemble(0.5*k(eigenmode, eigenmode))
    # Elastic energy in of the normed eigenmode
    E_elast_norm = E_elas / disp_z_mid**2.0

    print('Disp of middle = {}'.format(disp_z_mid))
    print('Energy = {}'.format(E_elas))
    print('Energy_norm = {}'.format(E_elast_norm))


    # plt.plot(eigenmode.vector())
    # plt.show()
    file_results.write(eigenmode, i) # Druhy argument nemusi byt int, muze byt cas.

    vtk_file = fe.File("./paraView/eigenmode_{}.pvd".format(i))
    vtk_file << (eigenmode)

# lam_vec and S_mat are ready now.
print("Vector of lambdas:")
print(lam_vec.shape)
print(lam_vec)
print("Matrix of selected eigenmodes:")
print(S_mat.shape)
print(S_mat)

# Modal stiffness matrix and modal mass matrix
k_mat = np.matmul(np.transpose(S_mat), np.matmul(K_array, S_mat))
m_mat = np.matmul(np.transpose(S_mat), np.matmul(M_array, S_mat))

# Print them
print("Modal stiffness matrix:")
print(k_mat.shape)
print(k_mat)
print("Modal mass matrix:")
print(m_mat.shape)
print(m_mat)

# Show them
plt.matshow(k_mat)
plt.title("Modal stiffness matrix k_mat")
plt.show()
plt.matshow(m_mat)
plt.title("Modal mass matrix m_mat")
plt.show()

print("Total mass: {} kg".format(total_mass))
print("Trace(m_mat): {} kg".format((np.trace(m_mat))))




lam_vec
S_mat.shape
plt.matshow(S_mat)
plt.show()

file_results.close()

